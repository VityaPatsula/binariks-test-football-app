﻿using Football.Core.Data;
using System.Collections.Generic;
using System.Linq;

namespace Football.Core.Logic
{
    public class Analizer
    {

        public static LeaguesStatistics GetLeaguesStatistic(IEnumerable<League> leagues)
        {
            var res = new LeaguesStatistics();

            Match productiveDay = null;
            List<Match> productiveDays = new List<Match>();

            foreach(var league in leagues)
            {
                var commands = new Dictionary<string, CommandScore>();
                foreach(var match in league.Matches.Where(x => x.Score != null)) // filter future matches
                {
                    #region Count Productive Days
                    if (productiveDay == null)
                    {
                        productiveDay = match;
                    }
                    else
                    {
                        var productiveDayGoals = productiveDay.GeneralGoals;
                        var currentDayGoals = match.GeneralGoals;

                        if (productiveDayGoals < currentDayGoals)
                        {
                            productiveDay = match;
                            productiveDays.Clear();
                            productiveDays.Add(match);
                        }
                        else if (productiveDayGoals == currentDayGoals)
                        {
                            productiveDays.Add(match);
                        }
                    }
                    #endregion

                    #region Count Goals / Missed for every command
                    if (!commands.TryGetValue(match.Team1, out var team1))
                    {
                        team1 = new CommandScore(match.Team1);
                        commands.Add(match.Team1, team1);
                    }

                    team1.GoalsScored += match.Team1Goals;
                    team1.MissedGoals += match.Team2Goals;

                    if (!commands.TryGetValue(match.Team2, out var team2))
                    {
                        team2 = new CommandScore(match.Team2);
                        commands.Add(match.Team2, team2);
                    }

                    team2.GoalsScored += match.Team2Goals;
                    team2.MissedGoals += match.Team1Goals;
                    #endregion
                }

                var attackers = commands.Values.Aggregate((current, next) => current.GoalsScored > next.GoalsScored ? current : next);
                var defenders = commands.Values.Aggregate((current, next) => current.MissedGoals < next.MissedGoals ? current : next);
                var bestRatio = commands.Values.Aggregate((current, next) =>
                {
                    var currentDifference = current.GoalsScored - current.MissedGoals;
                    var nextDifference = next.GoalsScored - next.MissedGoals;

                    if (currentDifference == nextDifference) // return command with bigger number of goals
                        return current.GoalsScored > next.GoalsScored ? current : next;

                    return currentDifference > nextDifference ? current : next;
                });

                var leagueScore = new LeagueScore(league.Name, attackers, defenders, bestRatio);
                res.Leagues.Add(leagueScore);
            }
            res.ProductiveDays = productiveDays;

            return res;
        }
    }
}
