﻿using Football.Core.Factory;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Football.Core.Managers
{
    public class DownloadManager
    {
        public async Task<List<string>> DownloadContentList(IEnumerable<string> links)
        {
            var dowloaded = new List<string>();
            using (var client = HttpClientFactory.GetClient())
            {
                HttpResponseMessage responce;
                string content;
                foreach (var link in links)
                {
                    responce = await client.GetAsync(link);
                    content = await responce.Content.ReadAsStringAsync();
                    dowloaded.Add(content);
                }
                client.Dispose();
            }
            return dowloaded;
        }
    }
}
