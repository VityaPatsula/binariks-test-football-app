﻿using Football.Core.Data.GitHub;
using Football.Core.Factory;
using Football.Core.Logic;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Football.Core.Managers
{
    public class GitHubManager
    {
        private const string url = "https://api.github.com/repos/openfootball/football.json/contents/2019-20";

        public async Task<List<GitFileInfo>> GitFileInfosAsync()
        {
            string content = string.Empty;
            using (var client = HttpClientFactory.GetClient())
            {
                var resp = await client.GetAsync(url);
                content = await resp.Content.ReadAsStringAsync();
                client.Dispose();
            }
            return Parser.ParseGitFileInfoList(content);
        }
    }
}
