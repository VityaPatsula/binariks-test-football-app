﻿using System.Net.Http;

namespace Football.Core.Factory
{
    public class HttpClientFactory
    {
        public static HttpClient GetClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent", "Football App");
            return client;
        }
    }
}
