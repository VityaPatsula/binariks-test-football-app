﻿using Newtonsoft.Json;

namespace Football.Core.Data.GitHub
{
    public class GitFileInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("download_url")]
        public string Link { get; set; }
    }
}
