﻿using System.Collections.Generic;

namespace Football.Core.Data
{

    public class LeaguesStatistics
    {
        public LeaguesStatistics()
        {
            Leagues = new List<LeagueScore>();
        }
        public List<LeagueScore> Leagues { get; set; }

        public List<Match> ProductiveDays { get; set; }
    }
}
