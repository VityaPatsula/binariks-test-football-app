﻿namespace Football.Core.Data
{
    public class CommandScore
    {
        public CommandScore(string name)
        {
            Name = name;
            GoalsScored = 0;
            MissedGoals = 0;
        }

        public string Name { get; private set; }
        public int GoalsScored { get; set; }
        public int MissedGoals { get; set; }
    }
}
