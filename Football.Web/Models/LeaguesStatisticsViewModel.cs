﻿using System.Collections.Generic;

namespace Football.Web.Models
{
    public class LeaguesStatisticsViewModel
    {
        public LeaguesStatisticsViewModel(List<LeagueScoreViewModel> leagues, List<MatchViewModel> productiveDays)
        {
            Leagues = leagues;
            ProductiveDays = productiveDays;
        }

        public List<LeagueScoreViewModel> Leagues { get; private set; }

        public List<MatchViewModel> ProductiveDays { get; private set; }
    }
}
