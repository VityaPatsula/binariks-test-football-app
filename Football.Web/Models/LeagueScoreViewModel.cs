﻿namespace Football.Web.Models
{
    public class LeagueScoreViewModel
    {
        public LeagueScoreViewModel(string name, CommandScoreViewModel attackers, CommandScoreViewModel defenders, CommandScoreViewModel bestRatio)
        {
            Name = name;
            Attackers = attackers;
            Defenders = defenders;
            BestRatio = bestRatio;
        }

        public string Name { get; private set; }
        public CommandScoreViewModel Attackers { get; private set; }
        public CommandScoreViewModel Defenders { get; private set; }
        public CommandScoreViewModel BestRatio { get; private set; }
    }
}
