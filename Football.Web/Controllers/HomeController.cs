﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Football.Web.Models;
using Microsoft.AspNetCore.Http;
using Football.Web.Extentions;
using Football.Core.Interfases;

namespace Football.Web.Controllers
{
    public class HomeController : Controller
    {
        private IFileService _fileService;

        public HomeController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(List<IFormFile> uploadedFiles)
        {
            if (!_fileService.ValidateFiles(uploadedFiles, out var message))
                return Error(message);

            var leguesStatistic = await _fileService.GetLeaguesStatistics(uploadedFiles);
            return View("LeguesStatisticView", leguesStatistic.ToViewModel());
        }

        public async Task<IActionResult> Index()
        {
            var files = await _fileService.GetFilesInfo();
            return View(files.ToViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> DownloadLinks(List<string> links)
        {
            var leguesStatistic = await _fileService.GetLeaguesStatistics(links);
            return View("LeguesStatisticView", leguesStatistic.ToViewModel());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(string message)
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier, Message = message });
        }
    }
}
